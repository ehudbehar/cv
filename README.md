# Curriculum vitae

 1. Write down a documentation page with all the knowledge behind this document.

 2. Find a way to insert a `vspace` only for non-first headings.

 3. Given the fact that I want a right-padding for the left minipage, the width of the left minipage should be changed so that the total width is 100% `textwidth`.